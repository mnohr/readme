The content of this was moved to the [GitLab handbook](https://about.gitlab.com/handbook/engineering/readmes/matt-nohr/)

---

- <img src="images/black-tanuki.png" alt="Black Lives Matter" width="30"/> I believe that [Black Lives Matter](https://blacklivesmatter.com/resource-category/toolkits/)
- <img src="images/rainbow-tanuki.png" alt="Pride" width="30"/> I am proud of and I support our [diverse workforce](https://about.gitlab.com/company/culture/inclusion/)
- [Indigenous Land Acknowledgement](land-acknowledgement.md)
