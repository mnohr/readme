## Indigenous Land Acknowledgement

Though I do not work in a traditional office building, my home is on land with a long history. 

According to [Northwestern University](https://www.northwestern.edu/native-american-and-indigenous-peoples/about/Land%20Acknowledgement.html): 

> It is important to understand the longstanding history that has brought you to reside on the land, and to seek to understand your place within that history. Land acknowledgements do not exist in a past tense, or historical context: colonialism is a current ongoing process, and we need to build our mindfulness of our present participation.

The earliest inhabitants of the area in which I live were the Dakota and the Ojibwe Indians who used it for their migratory hunting and harvesting grounds. The United States government designated the area as Dakota land in an 1825 treaty, but later purchased all Dakota territory east of the Mississippi to open it for European-American settlement. 

While I do not have the expertise or historic context to create my own land acknowledgement statement, I have collected land acknowledgement statements from organizations in my community as a way to acknowledge and learn about this history of the land.

### Ramsey County, Minnesota

Every community owes its existence and vitality to generations from around the world who contribute their hopes, dreams, and energy to making the history that led to this moment.

Some were brought here against their will, some were drawn to leave their distant homes in hope for a better life, and some have lived on this land since
time immemorial. Truth and acknowledgement are critical to building mutual respect and connection across barriers of heritage and difference.

Ramsey County is located on the ancestral lands of the Dakota People. We acknowledge the Ojibwe, the Ho Chunk, and the other nations of people who also called this place home. We pay respects to their elders, past and present, and consider their treaties made by the tribal nations that entitle nonNative people to live and work on traditional Native lands. We also consider the many legacies of violence, displacement, migration, and settlement that influence us to this day.

_This acknowledgement given in the U.S. Department of Arts and Culture Honor Native Land Guide – edited to reflect MN tribes. In review with St. Paul Indians in Action and endorsed by Shannon Geshick, Executive Director of the MN Indian Affairs Council._

### YMCA of the North

YMCA of the North Camps respectfully acknowledge that we are on the appropriated homelands of Indigenous peoples. We recognize the perseverance and survival of Indigenous communities, who continue to live throughout this region. Each camp is working to build sustainable relationships with our Indigenous communities and endeavor to be responsible stewards of the sacred nature of their homelands. This land acknowledgement is one of the ways in which we work to educate our camp communities about this land and our relationships with it and each other.

### University of Minnesota

We acknowledge that the University of Minnesota Twin Cities is built within the traditional homelands of the Dakota people. It is important to acknowledge the peoples on whose land we live, learn, and work as we seek to improve and strengthen our relations with our tribal nations. 

We also acknowledge that words are not enough. We must ensure that our institution provides support, resources, and programs that increase access to all aspects of higher education for our American Indian students, staff, faculty, and community members.

### More Information

- [A Guide to Indigenous Land Acknowledgement](https://nativegov.org/news/a-guide-to-indigenous-land-acknowledgment/) from the Native Governance Center
