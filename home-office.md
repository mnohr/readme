# Home Office Setup

Here is the equipment I have in my home office. This was acquired over a few years, mostly from remote work before I started at GitLab. If you want more information on any items, please let me know.

For GitLab-specific information, see the handbook links here:

- [Spending Company Money](https://about.gitlab.com/handbook/spending-company-money/)
- [Equipment Examples](https://about.gitlab.com/handbook/spending-company-money/equipment-examples/)


## Computer

- Laptop: [MacBook Pro, 13-inch, 2018](https://www.apple.com/macbook-pro-13/)
- Keyboard: [Apple Magic Keyboard](https://www.apple.com/shop/product/MLA22LL/A/magic-keyboard-us-english)
- Mouse: [Apple Magic Trackpad](https://www.apple.com/shop/product/MRMF2/magic-trackpad-2-space-gray)

## External Monitor

- Monitor: [Dell U2717D 27"](https://www.amazon.com/gp/product/B01D402Z28)
- Monitor Arm: [Amazon Basics](https://www.amazon.com/gp/product/B00MIBN16O)
- Webcam: [Logitech HD Pro Webcam C920](https://www.amazon.com/gp/product/B006JH8T3S)

## Audio Equipment

- Microphone: [Blue Snowball](https://www.amazon.com/gp/product/B014PYGTUQ/)
- Headphones: [Audio-Technica ATH-M20x](https://www.amazon.com/gp/product/B00HVLUR18)

## Desk

- Standing Desk: [Jarvis Standing Desk Frame](https://www.fully.com/standing-desks/desk-frames/jarvis-frame-only.html) with custom butcher-block desktop. This is an adjustable Sit/Stand desk but it is in the "standing" position almost 100% of the time
- Standing mat: [Topo Anti-Fatigue Mat](https://www.fully.com/accessories/standing-desk-mats/topo-standing-mat.html)
- Office chair: [HON chair](https://www.amazon.com/gp/product/B01DG9MXJC)
- Laptop stand: [Rain Stand](https://www.amazon.com/gp/product/B000OOYECC)
- USB Hub: [USB C Hub](https://www.amazon.com/gp/product/B07GDHD3VC) 
  - I use a USB hub so I can plug things like my microphone, headphones, power supply, and monitor into that and then just plug one cord into my laptop.
  - I choose this hub since it had PD charging (to pass though charging of my laptop while plugged in), an audio port for my headphones, and USB ports for my microphone, webcam, etc.
  - This also is helpful when switching to my personal laptop, since I can just plug the hub into that and everything is connected.
